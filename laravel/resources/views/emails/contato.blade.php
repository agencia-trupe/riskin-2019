<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
@if($telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone/WhatsApp:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Marca do Caminhão:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $caminhao_marca }}</span>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Modelo do Caminhão:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $caminhao_modelo }}</span>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Ano do Caminhão:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $caminhao_ano }}</span>
</body>
</html>
