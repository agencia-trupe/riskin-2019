<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ChamadasRequest;
use App\Http\Controllers\Controller;

use App\Models\Chamada;

class ChamadasController extends Controller
{
    public function index()
    {
        $registros = Chamada::ordenados()->get();

        return view('painel.chamadas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.chamadas.create');
    }

    public function store(ChamadasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Chamada::upload_icone();

            Chamada::create($input);

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Chamada $registro)
    {
        return view('painel.chamadas.edit', compact('registro'));
    }

    public function update(ChamadasRequest $request, Chamada $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Chamada::upload_icone();

            $registro->update($input);

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Chamada $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
