@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contrate</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.contrate.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.contrate.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
