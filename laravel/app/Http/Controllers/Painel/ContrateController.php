<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContrateRequest;
use App\Http\Controllers\Controller;

use App\Models\Contrate;

class ContrateController extends Controller
{
    public function index()
    {
        $registro = Contrate::first();

        return view('painel.contrate.edit', compact('registro'));
    }

    public function update(ContrateRequest $request, Contrate $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.contrate.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
