<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Home;
use App\Models\Banner;
use App\Models\Vantagem;

class HomeController extends Controller
{
    public function index()
    {
        $home      = Home::first();
        $banners   = Banner::ordenados()->get();
        $vantagens = Vantagem::ordenados()->get();

        return view('frontend.home', compact('home', 'banners', 'vantagens'));
    }
}
