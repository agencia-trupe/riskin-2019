@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::textarea('frase', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
