<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'contato@trupe.net',
            'telefone' => '11 2901 9686',
            'whatsapp' => '11 99402 1844',
            'endereco' => 'Rua Gomes de Carvalho, 1356, 2º andar - São Paulo - SP',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.219509308371!2d-46.68823068502158!3d-23.596459284665478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5748dc808035%3A0xe294fe2df77cb866!2sR.+Gomes+de+Carvalho%2C+1356+-+Vila+Ol%C3%ADmpia%2C+S%C3%A3o+Paulo+-+SP%2C+04547-000!5e0!3m2!1spt-BR!2sbr!4v1556071425817!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook' => 'https://www.facebook.com/riskinterceiros/',
            'instagram' => 'https://www.instagram.com/riskinterceiros/',
        ]);
    }
}
