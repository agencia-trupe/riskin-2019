<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('frase');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos');
    }
}
