@extends('frontend.common.template')

@section('content')

    <div class="como-funciona">
        <div style="overflow:hidden">
            <div class="center">
                <div class="titulo">
                    <h1>COMO FUNCIONA</h1>
                </div>
                <div class="texto">
                    {!! $comoFunciona->texto !!}
                </div>
            </div>
        </div>

        <div class="imagem">
            <div class="center">
                <img src="{{ asset('assets/img/layout/img-comofunciona.png') }}" alt="">
            </div>
        </div>
    </div>

@endsection
