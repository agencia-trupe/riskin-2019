<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'banner' => '<h2>Proteção para Terceiros</h2><p>Proteja seu <strong>caminhão</strong> contra prejuízos causados a terceiros.</p>',
            'frase' => 'Até <strong>40% mais barato</strong> que um seguro convencional.',
        ]);
    }
}
