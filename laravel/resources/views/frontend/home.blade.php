@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banner">
            <div class="center">
                <div class="banner-texto">
                    <h2>Proteção para Terceiros</h2>
                    <p>
                        Bateu com o <strong>caminhão</strong>
                        <span id="typed-banner">em algum carro?</span>
                        <br>
                        Sabia que existe uma proteção muito barata para esse tipo de situação?
                    </p>
                    <a href="{{ route('contato') }}">CONTRATE JÁ!</a>
                </div>
                <div class="banner-imagens">
                    @foreach($banners as $banner)
                    <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>

        <div class="frase">
            <div class="center">
                <p>{!! $home->frase !!}</p>
            </div>
        </div>

        <div class="vantagens">
            <div class="center">
                <h2>Vantagens da Proteção para Terceiros da Riskin</h2>

                <div class="vantagens-wrapper">
                    @foreach($vantagens as $vantagem)
                    <div class="vantagem">
                        <div class="titulo">
                            <img src="{{ asset('assets/img/vantagens/'.$vantagem->icone) }}" alt="">
                            {{ $vantagem->titulo }}
                        </div>
                        <p>{{ $vantagem->descricao }}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
