<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContrateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // 'banner' => 'required',
            'frase' => 'required',
            'sobre' => 'required',
        ];
    }
}
