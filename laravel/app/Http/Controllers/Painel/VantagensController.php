<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VantagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Vantagem;

class VantagensController extends Controller
{
    public function index()
    {
        $registros = Vantagem::ordenados()->get();

        return view('painel.vantagens.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.vantagens.create');
    }

    public function store(VantagensRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Vantagem::upload_icone();

            Vantagem::create($input);

            return redirect()->route('painel.vantagens.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Vantagem $registro)
    {
        return view('painel.vantagens.edit', compact('registro'));
    }

    public function update(VantagensRequest $request, Vantagem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Vantagem::upload_icone();

            $registro->update($input);

            return redirect()->route('painel.vantagens.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Vantagem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.vantagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
