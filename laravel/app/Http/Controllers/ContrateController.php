<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Contrate;
use App\Models\Banner;
use App\Models\Vantagem;

class ContrateController extends Controller
{
    public function index()
    {
        $contrate  = Contrate::first();
        $banners   = Banner::ordenados()->get();
        $vantagens = Vantagem::ordenados()->get();

        return view('frontend.contrate', compact('contrate', 'banners', 'vantagens'));
    }
}
