    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                {{ config('app.name') }}
            </a>

            <nav>
                @include('frontend.common.nav')
            </nav>

            <div class="contatos">
                <p class="telefone">
                    Fale conosco
                    <strong>{{ $contato->telefone }}</strong>
                </p>
                <a href="{{ $contato->whatsapp_link }}" class="whatsapp" target="_blank">
                    Fale pelo WhatsApp
                    <strong>{{ $contato->whatsapp }}</strong>
                </a>
            </div>
        </div>
    </header>
