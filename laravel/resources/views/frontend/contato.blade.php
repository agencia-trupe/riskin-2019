@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div style="overflow:hidden">
            <div class="center">
                <div class="left">
                    <div class="titulo">
                        <h1>CONTRATE JÁ</h1>
                    </div>

                    <div class="contatos">
                        <p class="telefone">
                            Fale conosco
                            <strong>{{ $contato->telefone }}</strong>
                        </p>
                        <a href="{{ $contato->whatsapp_link }}" class="whatsapp" target="_blank">
                            Fale pelo WhatsApp
                            <strong>{{ $contato->whatsapp }}</strong>
                        </a>
                    </div>
                </div>
                <div class="texto">
                    <p>Envie o formulário e um consultor comercial fará contato para realizar sua cotação:</p>

                    <form action="#" method="POST" data-route="{{ route('contatoPost') }}" id="form-contato">
                        <div class="form-wrapper">
                            <input type="text" name="nome" placeholder="nome" required>
                            <input type="email" name="email" placeholder="e-mail" required>
                            <input type="text" name="telefone" placeholder="telefone / whatsapp">
                            <input type="text" name="caminhao_marca" placeholder="marca do caminhão" required>
                            <input type="text" name="caminhao_modelo" placeholder="modelo do caminhão" required>
                            <input type="text" name="caminhao_ano" placeholder="ano do caminhão" required>
                            <button type="submit">OBTER<br>COTAÇÃO!</button>
                        </div>
                        <div class="form-response"></div>
                    </form>

                    <a href="{{ $contato->whatsapp_link }}" class="whatsapp" target="_blank">
                        <span></span>
                        Se preferir nos chame no WhatsApp
                        {{ $contato->whatsapp }}
                    </a>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="endereco">{{ $contato->endereco }}</div>
        </div>

        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

@endsection
