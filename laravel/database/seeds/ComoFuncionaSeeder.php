<?php

use Illuminate\Database\Seeder;

class ComoFuncionaSeeder extends Seeder
{
    public function run()
    {
        DB::table('como_funciona')->insert([
            'texto' => '<h2>Atuamos com garantia de prejuízos materiais causados a terceiros,</h2><p>oferecendo coberturas de R$ 100.000,00 e R$ 150.000,00 para pesados, com atendimento 24 horas em quaisquer ocorrências com envolvimento de terceiros.</p><p>Conduzimos o processo do início ao fim, desde a orientação no ato da ocorrência até a indenização final. Contamos com parceiros em nível nacional para nos atender de forma eficiente no reparo ou até mesmo reposição do patrimônio envolvido na ocorrência.</p>',
        ]);
    }
}
