import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

if ($('#typed-banner').length) {
  $('#typed-banner').html('');
  new Typed('#typed-banner', {
    strings: [
      'em algum carro?',
      'em algum poste?',
      'em algum portão?',
      'em algum muro?',
    ],
    typeSpeed: 60,
    backSpeed: 40,
    backDelay: 2500,
    smartBackspace: false,
    loop: true
  });
}

$('.banner-imagens').cycle({ timeout: 3000 });

$('#form-contato').submit(function(event) {
  event.preventDefault();

  const $form     = $(this);
  const $response = $form.find('.form-response');

  if ($form.hasClass('sending')) return;

  $form.addClass('sending');
  $response.fadeOut();

  $.post($form.data('route'), $form.serialize(), (data) => {
    $form.each(() => this.reset());
    $response.text('Mensagem enviada com sucesso!').fadeIn();
  }).fail((data) => {
    if (data.responseJSON) {
      let response = [];
      $.each(data.responseJSON, (field, errors) => {
        errors.map(error => response.push(error));
      });
      alert(response.join('\n'));
    } else {
      alert('Ocorreu um erro. Tente novamente.');
    }
  }).always(() => {
    $form.removeClass('sending');
  });
});
