    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">_ HOME</a>
                <a href="{{ route('quem-somos') }}">_ QUEM SOMOS</a>
                <a href="{{ route('como-funciona') }}">_ COMO FUNCIONA</a>
                <a href="{{ route('contato') }}">_ CONTRATE JÁ</a>
            </div>

            <div class="contatos">
                <p class="telefone">
                    Fale conosco
                    <strong>{{ $contato->telefone }}</strong>
                </p>
                <a href="{{ $contato->whatsapp_link }}" class="whatsapp" target="_blank">
                    Fale pelo WhatsApp
                    <strong>{{ $contato->whatsapp }}</strong>
                </a>
            </div>

            <div class="social">
                @foreach(['facebook', 'instagram'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endif
                @endforeach
                <a href="mailto:?subject=Conheça a Riskin&body=www.riskin.com.br" class="compartilhar">compartilhar</a>
            </div>

            <img src="{{ asset('assets/img/layout/marca-riskin-rodape.png') }}" alt="{{ config('app.name') }}" class="marca">

            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
