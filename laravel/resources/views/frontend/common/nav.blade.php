<a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>
    HOME
</a>
<a href="{{ route('quem-somos') }}" @if(Tools::routeIs('quem-somos')) class="active" @endif>
    QUEM SOMOS
</a>
<a href="{{ route('como-funciona') }}" @if(Tools::routeIs('como-funciona')) class="active" @endif>
    COMO FUNCIONA
</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
    CONTRATE JÁ
</a>
