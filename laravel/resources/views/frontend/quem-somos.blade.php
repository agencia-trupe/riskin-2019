@extends('frontend.common.template')

@section('content')

    <div class="quem-somos">
        <div style="overflow:hidden">
            <div class="center">
                <div class="titulo">
                    <h1>QUEM SOMOS</h1>
                </div>
                <div class="texto">
                    {!! $quemSomos->texto !!}
                </div>
            </div>
        </div>

        <div class="banner">
            <div class="center">
                <p>{!! $quemSomos->frase !!}</p>
                <img src="{{ asset('assets/img/layout/marca-riskin-sobre.png') }}" alt="">
            </div>
        </div>
    </div>

@endsection
