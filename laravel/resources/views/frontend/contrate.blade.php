@extends('frontend.common.template')

@section('content')

    <div class="contrate">
        <div class="banner">
            <div class="center">
                <div class="banner-texto">
                    <h2>Proteção para Terceiros</h2>
                    <p>
                        Bateu com o <strong>caminhão</strong>
                        <span id="typed-banner">em algum carro?</span>
                        <br>
                        Sabia que existe uma proteção muito barata para esse tipo de situação?
                    </p>
                    <a href="{{ route('contato') }}">COTAÇÃO GRÁTIS</a>
                </div>
                <div class="detalhe-diagonal"></div>
                <div class="banner-imagens">
                    @foreach($banners as $banner)
                    <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>

        <div class="contrate-contato">
            <div class="center">
                <div class="chamada">
                    <p>
                        {!! $contrate->frase !!}
                    </p>
                </div>

                <div class="wrapper">
                    <form action="#" method="POST" data-route="{{ route('contatoPost') }}" id="form-contato">
                        <div class="form-wrapper">
                            <input type="text" name="nome" placeholder="nome" required>
                            <input type="email" name="email" placeholder="e-mail" required>
                            <input type="text" name="telefone" placeholder="telefone / whatsapp">
                            <input type="text" name="caminhao_marca" placeholder="marca do caminhão" required>
                            <input type="text" name="caminhao_modelo" placeholder="modelo do caminhão" required>
                            <input type="text" name="caminhao_ano" placeholder="ano do caminhão" required>
                            <button type="submit">OBTER<br>COTAÇÃO!</button>
                        </div>
                        <div class="form-response"></div>
                    </form>
                </div>
            </div>
        </div>

        <div class="vantagens">
            <div class="center">
                <h2>Vantagens da Proteção para Terceiros da Riskin</h2>

                <div class="vantagens-wrapper">
                    @foreach($vantagens as $vantagem)
                    <div class="vantagem">
                        <div class="titulo">
                            <img src="{{ asset('assets/img/vantagens/'.$vantagem->icone) }}" alt="">
                            {{ $vantagem->titulo }}
                        </div>
                        <p>{{ $vantagem->descricao }}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="sobre">
            <div class="center">
                <img src="{{ asset('assets/img/layout/marca-riskin-sobre.png') }}" alt="">
                <div class="sobre-texto">
                    <h2>SOBRE A RISKIN</h2>
                    <p>{!! $contrate->sobre !!}</p>
                    <a href="{{ route('quem-somos') }}">
                        SAIBA MAIS &raquo;
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
