<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Chamada extends Model
{
    protected $table = 'chamadas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'       => 75,
            'height'      => 75,
            'transparent' => true,
            'path'        => 'assets/img/chamadas/'
        ]);
    }
}
