<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContrateTable extends Migration
{
    public function up()
    {
        Schema::create('contrate', function (Blueprint $table) {
            $table->increments('id');
            $table->text('banner');
            $table->text('frase');
            $table->text('sobre');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contrate');
    }
}
