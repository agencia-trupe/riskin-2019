<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'Riskin &middot; Thinking Beyond',
        ]);
    }
}
