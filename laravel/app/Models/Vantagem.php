<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Vantagem extends Model
{
    protected $table = 'vantagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'       => 50,
            'height'      => 50,
            'transparent' => true,
            'path'        => 'assets/img/vantagens/'
        ]);
    }
}
