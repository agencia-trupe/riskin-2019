@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vantagens /</small> Editar Vantagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.vantagens.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.vantagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
