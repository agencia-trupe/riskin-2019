<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ComoFunciona;

class ComoFuncionaController extends Controller
{
    public function index()
    {
        $comoFunciona = ComoFunciona::first();

        return view('frontend.como-funciona', compact('comoFunciona'));
    }
}
