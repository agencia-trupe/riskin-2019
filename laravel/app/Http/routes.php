<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('como-funciona', 'ComoFuncionaController@index')->name('como-funciona');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contatoPost');
    Route::get('contrate', 'ContrateController@index')->name('contrate');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('banners', 'BannersController');
		Route::resource('vantagens', 'VantagensController');
		Route::resource('contrate', 'ContrateController', ['only' => ['index', 'update']]);
		// Route::resource('chamadas', 'ChamadasController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('como-funciona', 'ComoFuncionaController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
