@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('icone', 'Ícone') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/vantagens/'.$registro->icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('icone', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.vantagens.index') }}" class="btn btn-default btn-voltar">Voltar</a>
