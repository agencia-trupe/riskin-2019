@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vantagens /</small> Adicionar Vantagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.vantagens.store', 'files' => true]) !!}

        @include('painel.vantagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
