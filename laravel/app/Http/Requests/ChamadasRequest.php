<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'icone' => 'required|image',
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['icone'] = 'image';
        }

        return $rules;
    }
}
