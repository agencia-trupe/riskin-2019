<?php

use Illuminate\Database\Seeder;

class ContrateSeeder extends Seeder
{
    public function run()
    {
        DB::table('contrate')->insert([
            'banner' => '<h2>Proteção para Terceiros</h2><p>Bateu com o <strong>caminhão</strong> em algum carro na rua?<br>Sabia que existe uma proteção muito barata para esse tipo de situação?</p>',
            'frase' => 'Preencha os campos e receba uma proposta personalizada até <strong>40% mais barata</strong> que um seguro convencional.',
            'sobre' => 'A Riskin trabalha <strong>somente</strong> com Proteção de Terceiros para <strong>caminhões</strong> com total segurança e assistência nesse tipo de ocorrência.',
        ]);
    }
}
