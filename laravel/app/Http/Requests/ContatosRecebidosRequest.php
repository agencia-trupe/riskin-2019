<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'            => 'required',
            'email'           => 'required|email',
            'caminhao_marca'  => 'required',
            'caminhao_modelo' => 'required',
            'caminhao_ano'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome.required'            => 'Preencha seu nome.',
            'email.required'           => 'Preencha seu e-mail.',
            'email.email'              => 'Insira um endereço de e-mail válido.',
            'caminhao_marca.required'  => 'Preencha a marca do caminhão.',
            'caminhao_modelo.required' => 'Preencha o modelo do caminhão.',
            'caminhao_ano.required'    => 'Preencha o ano do caminhão.',
        ];
    }
}
