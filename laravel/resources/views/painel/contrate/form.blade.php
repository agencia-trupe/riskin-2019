@include('painel.common.flash')

{{-- <div class="form-group">
    {!! Form::label('banner', 'Banner') !!}
    {!! Form::textarea('banner', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div> --}}

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::textarea('frase', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('sobre', 'Sobre') !!}
    {!! Form::textarea('sobre', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
