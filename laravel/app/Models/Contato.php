<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contato';

    protected $guarded = ['id'];

    public function getWhatsappLinkAttribute()
    {
        $url      = 'https://api.whatsapp.com/send?phone=55';
        $whatsapp = str_replace(' ', '', $this->whatsapp);

        return $url.$whatsapp;
    }
}
